package com.application.classifiedproject;

import com.application.classifiedproject.domain.Classified;
import com.application.classifiedproject.service.ClassifiedService;
import com.application.classifiedproject.service.ClassifiedServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;



@RunWith(SpringRunner.class)
@SpringBootTest
public class ClassifiedprojectApplicationTests {

	@Autowired
	private ClassifiedService classifiedService;

	@Test
	public void testCountWordsServiceMethodWithMultipleSpaces() {

		Classified testClassified = new Classified("  new  classified   ");

		Classified resultClassified = classifiedService.countWords(testClassified);

		assertNotNull(resultClassified);
		assertEquals(testClassified.getText(), resultClassified.getText());
		assertEquals(2, resultClassified.getWordsNumber());

	}

	@Test
	public void testCountWordsServiceMethodWithMultipleSpacesAndOneCharacterWords() {

		Classified testClassified = new Classified("  n e w  c l a s s i f i e d    ");

		Classified resultClassified = classifiedService.countWords(testClassified);

		assertNotNull(resultClassified);
		assertEquals(testClassified.getText(), resultClassified.getText());
		assertEquals(0, resultClassified.getWordsNumber());
		assertFalse(resultClassified.isContainsEuroAmount());
	}

	@Test
	public void testCountWordsServiceMethodWithFirstClassifiedText() {

		String textOfClassified = "Μεσιτικη εταιρεια στα πλαισια της διαρκους αναπτυξης της ζητει \"Assistant Manager\", προσφεροντας ενα ιδιαιτερα ελκυστικο πακετο αμοιβων, μεγαλες προοπτικες περαιτερω εξελιξης. Ο ιδανικος υποψηφιος/α, θα πρεπει να διαθετει: εμπειρια στον κλαδο των ακινητων ή της διαχειρισης ανθρωπινου δυναμικου, οργανωτικο πνευμα, και διαπραγματευτικες ικανοτητες Τηλ επικοινωνιας 6999999999(153) 400€";

		Classified testClassified = new Classified(textOfClassified);

		Classified resultClassified = classifiedService.countWords(testClassified);

		assertNotNull(resultClassified);
		assertEquals(testClassified.getText(), resultClassified.getText());
		assertEquals(45, resultClassified.getWordsNumber());
		assertTrue(resultClassified.isContainsEuroAmount());
	}

	@Test
	public void testCountWordsServiceMethodWithSecondClassifiedText() {

		String textOfClassified = "ΖΗΤΟΥΝΤΑΙ υπάλληλοι σε καταστήματα πώλησης παραδοσιακών προϊόντων και καλλυντικών για την περίοδο Ιουνίου έως και Σεπτεμβρίου (4 Μήνες). Απαραίτητη γνώση της Αγγλικής γλώσσας, γνώση ρωσικής γλώσσας θα εκτιμηθεί. Διαμονή πληρωμένη σε διαμέρισμα κοντά στο χώρο εργασίας. Μισθός 800 ευρώ. email: info@here.gr";

		Classified testClassified = new Classified(textOfClassified);

		Classified resultClassified = classifiedService.countWords(testClassified);

		assertNotNull(resultClassified);
		assertEquals(testClassified.getText(), resultClassified.getText());
		assertEquals(41, resultClassified.getWordsNumber());
		assertTrue(resultClassified.isContainsEuroAmount());

	}

	@Test
	public void testCountWordsServiceMethodWithThirdClassifiedText() {

		String textOfClassified = "ΚΥΨΕΛΗ διαμέρισμα 60 τ.μ., 1ου, προσόψεως, διαμπερές, 1 υ/δ, μπάνιο, ξεχωριστή κουζίνα, κλιματισμός, πόρτα ασφαλείας, ελεύθερο, ανακαινισμένο, εξαιρετικό πλήρως ανακαινισμένο διαμέρισμα σε ήσυχη περιοχή, αποτελείται από ένα φωτεινότατο σαλόνι, ξεχωριστή ανακαινισμένη κουζίνα, ανακαινισμένο μπάνιο. Διπλά τζάμια, ανακαινισμένη κουζίνα, μπάνιο, πατώματα. Ανακαινισμένα ηλεκτρικά και υδραυλικά. Η ανακαίνιση έγινε το 2019. Μόλις 4 λεπτά από τα δικαστήρια στην Ευελπίδων. Κατάλληλο και για επαγγελματική χρήση.";

		Classified testClassified = new Classified(textOfClassified);

		Classified resultClassified = classifiedService.countWords(testClassified);

		assertNotNull(resultClassified);
		assertEquals(testClassified.getText(), resultClassified.getText());
		assertEquals(59, resultClassified.getWordsNumber());
		assertFalse(resultClassified.isContainsEuroAmount());

	}

}
