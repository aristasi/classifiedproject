package com.application.classifiedproject;

import com.application.classifiedproject.controller.ClassifiedController;
import com.application.classifiedproject.domain.Classified;
import com.application.classifiedproject.service.ClassifiedService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.Assert.*;


import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class ClassifiedRestControllerTests {

    @InjectMocks
    private ClassifiedController classifiedController;

    @Mock
    private ClassifiedService classifiedService;

    @Test
    public void ClassifiedControllerTest(){
        Classified classified = new Classified("new classified");
        Classified resultClassified = new Classified("new classified", 2, false);

        when(classifiedService.countWords(classified)).thenReturn(resultClassified);

        Classified returnedClassified = classifiedController.insert(resultClassified);

        verify(classifiedService).countWords(resultClassified);

    }

}


