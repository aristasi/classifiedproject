package com.application.classifiedproject.service;

import com.application.classifiedproject.domain.Classified;
import org.springframework.stereotype.Service;

@Service
public class ClassifiedServiceImpl implements ClassifiedService{

    @Override
    public Classified countWords(Classified classified) {

        String textOfClassified = classified.getText();
        int count = numberOfWordsOfAClassified(textOfClassified);

        Classified classifiedToReturn = checkIfClassifiedContainsEuroAmount(classified, textOfClassified, count);

        return classifiedToReturn;
    }

    private Classified checkIfClassifiedContainsEuroAmount(Classified classified, String textOfClassified, int count) {
        String euroAmount = "ευρώ";
        String euroAmountTwo = "ευρω";
        Classified classifiedToReturn = new Classified(classified.getText(),count, false);

        if (textOfClassified.toLowerCase().contains("€") || textOfClassified.toLowerCase().contains(euroAmount) || textOfClassified.toLowerCase().contains(euroAmountTwo)){

            classifiedToReturn.setContainsEuroAmount(true);
            return classifiedToReturn;
        }

        return classifiedToReturn;
    }


    private int numberOfWordsOfAClassified(String input){
        int numberOfWords = 0;

        if (input.length()<2){
            return numberOfWords;
        }

        String[] words = input.trim().replaceAll("\\s+"," ").split("\\s");
        for (String word : words) {
            word.replace(" ", "");
            if (word.length() > 1) {
                numberOfWords = numberOfWords + 1;
            }
        }
        return numberOfWords;
    }
}

