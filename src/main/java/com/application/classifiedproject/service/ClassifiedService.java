package com.application.classifiedproject.service;
import com.application.classifiedproject.domain.Classified;


public interface ClassifiedService {

    Classified countWords(Classified classified);

}
