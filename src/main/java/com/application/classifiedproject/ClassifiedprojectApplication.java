package com.application.classifiedproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassifiedprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassifiedprojectApplication.class, args);
	}

}
