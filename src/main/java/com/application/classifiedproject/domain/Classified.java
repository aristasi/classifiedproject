package com.application.classifiedproject.domain;

public class Classified {

    private String text;
    private int wordsNumber;
    private boolean containsEuroAmount;

    public Classified(){

    }

    public Classified(String text) {
        this.text = text;
    }

    public Classified(String text, int wordsNumber) {
        this.text = text;
        this.wordsNumber = wordsNumber;
    }

    public Classified(String text, int wordsNumber, boolean containsEuroAmount) {
        this.text = text;
        this.wordsNumber = wordsNumber;
        this.containsEuroAmount = containsEuroAmount;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getWordsNumber() {
        return wordsNumber;
    }

    public void setWordsNumber(int wordsNumber) {
        this.wordsNumber = wordsNumber;
    }

    public boolean isContainsEuroAmount() {
        return containsEuroAmount;
    }

    public void setContainsEuroAmount(boolean containsEuroAmount) {
        this.containsEuroAmount = containsEuroAmount;
    }

    @Override
    public String toString() {
        return "Classified{" +
                "text='" + text + '\'' +
                ", wordsNumber=" + wordsNumber +
                ", containsEuroAmount=" + containsEuroAmount +
                '}';
    }
}
