package com.application.classifiedproject.controller;


import com.application.classifiedproject.domain.Classified;
import com.application.classifiedproject.service.ClassifiedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/classifieds")
public class ClassifiedController {

    @Autowired
    ClassifiedService classifiedService;

    @PostMapping("/count")
    public Classified insert(@RequestBody Classified classified) {
        return classifiedService.countWords(classified);
    }

}
